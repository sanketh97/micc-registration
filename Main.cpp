#include "Main_includes.hpp"

// K is defined, and should be changed, in Main_includes.hpp
typedef K::Point_2 Point_2;
typedef K::Point_3 Point_3;
typedef K::Plane_3 Plane_3;
typedef K::Vector_3 Vector_3;
typedef K::Sphere_3 Sphere_3;
typedef K::Segment_3 Segment_3;
typedef K::Direction_3 Direction_3;
typedef K::Point_2 Point_2;
typedef CGAL::Aff_transformation_3<K> Transformation_3;
typedef K::FT FT;
typedef CGAL::Polyhedron_3<K> Polyhedron_3;
typedef CGAL::Polyhedron_3<K>::Vertex Vertex;
typedef CGAL::Polyhedron_3<K>::Vertex_iterator Vertex_iterator;
typedef CGAL::Polyhedron_3<K>::Vertex_handle Vertex_handle;

// Any changes to EnrichedPoint should be made in projection.cpp too
/*index, Point_3, normal, Polyhedron Vertex, projection cell, isContour, isO, color*/
typedef boost::tuple<int, Point_3, Vector_3, Vertex_handle, Cell<K>*, bool, bool, boost::tuple<int, int, int>> EnrichedPoint;
typedef std::vector<EnrichedPoint> EPVec;

// Globals
const Point_3 point_at_infinity(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity());

int test1()
{
    Point_2 points[5] = { Point_2(0,0), Point_2(10,0), Point_2(10,10), Point_2(6,5), Point_2(4,1) };
    Point_2 result[5];
    Point_2 *ptr = CGAL::convex_hull_2( points, points+5, result );
    std::cout << ptr - result << " points on the convex hull:" << std::endl;
    for(int i = 0; i < ptr - result; i++)
    {
        std::cout << result[i] << std::endl;
    }
    return 0;
}

int test2()
{
    Point_3 point(1.0, 2.0, 1.543);
    Point_3 p1(0, -1, -1);
    Point_3 p2(0, 1, -1);
    Point_3 p3(0, 0, 1);

    Plane_3 plane1(p1, p2, p3); // direction toward +ve x-axis
    Plane_3 plane2(p1, p3, p2); // direction toward -ve x-axis
    std::cout << "plane1 " << plane1.oriented_side(point) << std::endl;
    std::cout << "plane2 " << plane2.oriented_side(point) << std::endl;
    // conclusion: direction pointed to is +ve
    return 0;
}

int test_cull()
{
    std::vector<Point_3> p;
    Point_3 p1(0, 0, 0), p2(1, 1, 1), p3(7, 8, 9), p4(-100, -100, -1000.2);
    p.push_back(p1);
    p.push_back(p2);
    p.push_back(p3);
    p.push_back(p4);

    std::vector<Plane_3> planes;
    Point_3 a(2, 2, 2), b(2, 2, 0), c(2, 0, 2), d(0, 2, 2);
    Plane_3 plane1(a, b, c), plane2(a, c, d), plane3(a, d, b);
    planes.push_back(plane1);
    planes.push_back(plane2);
    planes.push_back(plane3);

    std::vector<Point_3>* culled = cull<Point_3, Plane_3>(p, planes);
    std::copy(culled->cbegin(), culled->cend(), std::ostream_iterator<Point_3>(std::cout, ",\n"));

    return 0;
}

int test_3()
{
    std::ifstream file("test.txt", std::ifstream::in);
    Point_3 p;
    file >> p;
    std::cout << p;
    return 0;
}

int test_translate()
{
    std::vector<Point_3> v;
    v.push_back(Point_3(0, 0, 0));
    std::vector<Point_3>* result = translate(v, Vector_3(1, 1, 2.30));
//    DEBUG((Point_3(0, 1, 0) + Vector_3(1, 2, 3.20)));
    print_container(*result);
    return 0;
}

int prep(std::string input, std::string output)
{
	EPVec v;
	std::ifstream stream(input);
	if (!stream ||
			!CGAL::read_xyz_points(stream, std::back_inserter(v),
					CGAL::Nth_of_tuple_property_map<1, EnrichedPoint>()))
	{
		std::cerr << "Error: cannot read file " << input << std::endl;
		return EXIT_FAILURE;
	}

    // cull
    Plane_3 plane1(Point_3(0, -1, 1.6), Point_3(-1, 0, 1.6), Point_3(1, 0, 1.6));
    Plane_3 plane2(Point_3(0.5, 1, 0), Point_3(0.5, 0, -1), Point_3(0.5, 0, 1));
    Plane_3 plane3(Point_3(-0.7, 1, 0), Point_3(-0.7, 0, 1), Point_3(-0.7, 0, -1));
    Plane_3 plane4(Point_3(0, -1, 0.8), Point_3(1, 0, 0.8), Point_3(-1, 0, 0.8));
    Plane_3 floor_plane(Point_3(0, -0.3, -1), Point_3(0, -0.3, 1), Point_3(1, -0.3, 1));
    std::vector<Plane_3> planes;
    planes.push_back(plane1);
    planes.push_back(plane2);
    planes.push_back(plane3);
    planes.push_back(plane4);
    planes.push_back(floor_plane);

    EPVec culled;
//    std::vector<Point_3> culled;
    cull(v.cbegin(), v.cend(), CGAL::Nth_of_tuple_property_map<1, EnrichedPoint>(), planes.cbegin(), planes.cend(),
    		std::back_inserter(culled), CGAL::Nth_of_tuple_property_map<1, EnrichedPoint>());

/*    // smooth
    std::vector<Point_3> smoothed;
    CGAL::jet_smooth_point_set(culled.begin(), culled.end(), 24);*/

    // translate
    Vector_3 translate_vector(0, 0, -1.35);
    EPVec translated;
//    std::vector<Point_3> translated;
    translate(CGAL::Nth_of_tuple_property_map<1, EnrichedPoint>(), culled.cbegin(), culled.cend(),
    		CGAL::Nth_of_tuple_property_map<1, EnrichedPoint>(),
    		std::back_inserter(translated), translate_vector);

/*    // rotate
    Transformation_3 t(0.7071, 0.7071, 0, -0.7071, 0.7071, 0, 0, 0, 1);
    std::vector<Point_3> rotated;

    rotate(CGAL::Identity_property_map<Point_3>(), translated.cbegin(), translated.cend(), CGAL::Identity_property_map<Point_3>(),
    		std::back_inserter(rotated), t);*/


    auto map1 = CGAL::Nth_of_tuple_property_map<1, EnrichedPoint>();
    auto map2 = CGAL::Nth_of_tuple_property_map<2, EnrichedPoint>();

    // Write only points
	std::ofstream file(output, std::ofstream::out);
	if (!file
			|| !CGAL::write_xyz_points(file, translated.cbegin(),
					translated.cend(), map1)) {
		std::cerr << "Error: Cannot write to file " << output << std::endl;
		return EXIT_FAILURE;
	}

	return 0;
}

void prep_default()
{
	std::string files[] = { "points1_right.xyz", "points2_backright.xyz", "points3_left.xyz",
			"points4_left.xyz", "points5_back.xyz", "points6_backleft.xyz",
			"points7_frontleft.xyz", "points8_frontright.xyz" };
	for (int i = 0; i < 8; i++)
	{
		prep(std::string("../data/") + files[i], std::string("../data/prepped/") + files[i]);
	}
}

void make_polyhedron_compatible(std::string input, std::string output)
{
	Polyhedron_3 p;

	std::ifstream in_file(input, std::ifstream::in);
	CGAL::scan_OFF(in_file, p, true);
	in_file.close();

	std::cout << "Read " << p.size_of_facets() << " facets from " << input << std::endl;
	std::ofstream file(output, std::ofstream::out);
	file << p;
	file.close();
}

void make_polyhedron_compatible_default()
{
	std::string files[] = { "points1_right.xyz.jv", "points2_backright.xyz.jv", "points3_left.xyz.jv",
			"points4_left.xyz.jv", "points5_back.xyz.jv", "points6_backleft.xyz.jv",
			"points7_frontleft.xyz.jv", "points8_frontright.xyz.jv" };
	for (int i = 0; i < 8; i++)
	{
		make_polyhedron_compatible(std::string("../data/coconed/") + files[i], std::string("../data/polyhedron_compatible/") + files[i] + ".off");
	}
}

Polyhedron_3 read_polyhedron(std::string input)
{
	Polyhedron_3 p;
	std::ifstream in_file(input, std::ifstream::in);
	CGAL::scan_OFF(in_file, p, true);
	in_file.close();
	std::cout << "Read " << p.size_of_facets() << " facets and " << p.size_of_vertices() << " vertices from " << input << std::endl;
	return p;
}

std::vector<Polyhedron_3> read_polyhedron_default()
{
	std::string files[] = { "points1_right.xyz.jv.off", "points2_backright.xyz.jv.off", "points3_left.xyz.jv.off",
				"points4_left.xyz.jv.off", "points5_back.xyz.jv.off", "points6_backleft.xyz.jv.off",
				"points7_frontleft.xyz.jv.off", "points8_frontright.xyz.jv.off" };
	std::vector<Polyhedron_3> p;
	for (int i = 0; i < 1; i++)
	{
		p.push_back(read_polyhedron(std::string("../data/polyhedron_compatible/") + files[i]));
	}
	return p;
}

// todo Use property map instead of boost::get<>(), take EPVec& as input
EPVec extract_points_from_polyhedron(Polyhedron_3& p)
{
	EPVec vec;
	for (Vertex_iterator vit = p.vertices_begin(); vit != p.vertices_end(); vit++)
	{
		EnrichedPoint ep;
		boost::get<1>(ep) = vit->point();
		boost::get<3>(ep) = vit;
		vec.push_back(ep);
	}
//	std::cout << "foobar" << std::endl;
	return vec;
}

template <typename Iterator, class Point_Map_In, class Normal_Map_Out>
void compute_normals(Iterator begin, Iterator end, Point_Map_In pmap, Normal_Map_Out nmap)
{
	// Calculate normals
	CGAL::jet_estimate_normals(begin, end,
			pmap, nmap, 18);

	// Orient normals
	CGAL::mst_orient_normals(begin, end,
			pmap, nmap, 18);
}

void test_cgal_infinity_handling()
{
	std::cout << Point_3(0, 0, std::numeric_limits<double>::infinity()) << std::endl;
	Segment_3 s(Point_3(0, 0, 0), Point_3(0, 0, std::numeric_limits<double>::infinity()));
	Plane_3 plane(Point_3(0, 0, 0), Point_3(1, 1, 0), Point_3(-1, -1, 0));
	auto result = CGAL::intersection(plane, s);
	std::cout << result << std::endl;
	// Conclusion: doesn't handle infinities
}

void test_change_camera()
{
	Polyhedron_3 tetrahedron;
	tetrahedron.make_tetrahedron(Point_3(0, 1, 0), Point_3(-1, -1, 0), Point_3(1, -1, 0), Point_3(0, 0, 1));
	std::ofstream tetra1("tetra1.off", std::ofstream::out);
	tetra1 << tetrahedron;
	tetra1.close();
	change_camera<K>(tetrahedron.points_begin(), tetrahedron.points_end(),
			CGAL::Identity_property_map<Point_3>(), CGAL::Identity_property_map<Point_3>(),
			Point_3(0, 0, 0), get_old_x_direction<K>(45.0), Direction_3(0, 1, 0));
	std::ofstream tetra2("tetra2.off", std::ofstream::out);
	tetra2 << tetrahedron;
	tetra2.close();
}

void test_make_grid()
{
//	std::vector<Cell<K>*>* grid = make_grid(Point_3(-0.42, 0.28, 0), 0.6, 0.6, 2, 2, 7);
	std::vector<Cell<K>*> grid = make_grid(Point_3(0, 4, 0), 4, 4, 2, 2, 2);
	std::ofstream g("grid.xyz", std::ofstream::out);
	for (auto it = grid.begin(); it != grid.end(); it++)
	{
		g << (*it)->top_left << std::endl;
//		std::cout << (*it)->top_left << " depth: " << (*it)->depth << std::endl;
	}
	g.close();
	Cell<K>* root = find_root(grid[0]);
	std::cout << root->top_left << " " << root->depth << std::endl;
}

void test_assign_elements_to_cells()
{
//	std::vector<Cell<K>*> grid = make_grid(Point_3(0, 4, 0), 4, 4, 2, 2, 2);
//	Polyhedron_3 p = read_polyhedron("gridtest.off");
	std::vector<Cell<K>*> grid = make_grid(Point_3(-0.42, 0.28, 0), 0.6, 0.6, 2, 2, 8);
//	Polyhedron_3 p = read_polyhedron("../data/polyhedron_compatible/points1_right.xyz.jv.off");
	Polyhedron_3 p = read_polyhedron("../data/polyhedron_compatible/points2_backright.xyz.jv.off");

	EPVec epvec = extract_points_from_polyhedron(p);
	CGAL::Nth_of_tuple_property_map<1, EnrichedPoint> pmap;
	CGAL::Nth_of_tuple_property_map<4, EnrichedPoint> cmap;

	assign_facets_to_cells<K>(p, grid[0]);
//	int ch = std::cin.get();
	assign_vertices_to_cells(epvec.begin(), epvec.end(), pmap, cmap, grid[0]);

	std::ofstream file("cell_assignments.txt", std::ofstream::out);
	int empty = 0;
	int total = 0;
	int max_num_points = 0;
	for (int i = 0; i < grid.size(); i++)
	{
//		std::cout << "i: " << i << ", num facets: " << grid[i]->contained_facets.size() << ", top_left: " << grid[i]->top_left << std::endl;
		int num_points = grid[i]->contained_points.size();
		total += num_points;
		if (!num_points)
			empty++;
		file << "i: " << i << ", num points: " << num_points << ", top_left: " << grid[i]->top_left << std::endl;
		if (max_num_points < num_points)
		{
			max_num_points = num_points;
		}
	}
	file << "total_points: " << total << ", empty_cells: " << empty << ", max_num_points: " << max_num_points << std::endl;
	file.close();

	std::ofstream file2("projecte.xyz", std::ofstream::out);
	for (auto it = epvec.cbegin(); it != epvec.cend(); it++)
	{
		if (get(cmap, *it))
		{
			Point_3 p = get(pmap, *it);
			file2 << Point_3(p.x(), p.y(), 0) << std::endl;
		}
	}
	file2.close();
}

int main()
{
	test_assign_elements_to_cells();
//	test_make_grid();
	return 0;
	std::vector<Cell<K>*> grid = make_grid(Point_3(0, 8, 0), 8, 8, 2, 2, 3);
	auto grid_2d = make_2d_vector_of_grid_leaves<K>(grid.cbegin(), grid.cend(), 8, 8);
	for (auto it1 = grid_2d.cbegin(); it1 != grid_2d.cend(); it1++)
	{
		for (auto it2 = it1->cbegin(); it2 != it1->cend(); it2++)
		{
			std::cout << (*it2)->row_index << " " << (*it2)->column_index << "\t";
		}
		std::cout << std::endl;
	}
	return 0;
	std::for_each(grid.cbegin(), grid.cend(), [](decltype(*(grid.cbegin())) c){ std::cout << c->top_left << " " << c->column_index << ", " << c->row_index << std::endl; });
	return 0;
//	test_change_camera();

//	prep_default();
//	make_polyhedron_compatible_default();
	std::vector<Polyhedron_3> p = read_polyhedron_default();
	std::cout << "vertices: " << p[0].size_of_vertices() << " facets: " << p[0].size_of_facets() << std::endl;
	EPVec epv0 = extract_points_from_polyhedron(p[0]);
	CGAL::Nth_of_tuple_property_map<1, EnrichedPoint> point_map;
	CGAL::Nth_of_tuple_property_map<2, EnrichedPoint> normal_map;
	CGAL::Nth_of_tuple_property_map<3, EnrichedPoint> vertex_map;
	CGAL::Nth_of_tuple_property_map<4, EnrichedPoint> projection_map;
	std::cout << range(epv0.begin(), epv0.end(), point_map) << std::endl;
	compute_normals(epv0.begin(), epv0.end(), point_map, normal_map);
	return 0;
}
