/*
 * Modified_polyhedron_scan_OFF.h
 *
 *  Created on: 04-May-2015
 *      Author: chinmaya
 */

#ifndef MODIFIED_POLYHEDRON_SCAN_OFF_H_
#define MODIFIED_POLYHEDRON_SCAN_OFF_H_

#include <CGAL/basic.h>
#include <CGAL/IO/File_header_OFF.h>
#include <CGAL/IO/File_scanner_OFF.h>
#include <CGAL/Modifier_base.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/print_OFF.h>
#include <iostream>
#include <cstddef>
#include <vector>

namespace CGAL {

template < class HDS>
class Polyhedron_scan_OFF :  public Modifier_base<HDS> {
protected:
    std::istream&    m_in;
    File_header_OFF  m_file_header;
public:

    typedef HDS Halfedge_data_structure;

// DEFINITION
//
// Polyhedron_scan_OFF<Traits> is a polyhedral surface builder.
// It scans a polyhedron given in OFF from a stream and appends it
// incrementally using the incremental builder.

    Polyhedron_scan_OFF( std::istream& in, bool verbose = false)
        : m_in(in), m_file_header( verbose) {}

    // Activation
    void operator()( HDS& hds);

    const File_header_OFF&  header() const { return m_file_header; }
};

template < class HDS >
void
Polyhedron_scan_OFF<HDS>:: operator()( HDS& target) {
    File_scanner_OFF scanner( m_in, m_file_header.verbose());
    if ( ! m_in) {
        if ( scanner.verbose()) {
            std::cerr << " " << std::endl;
            std::cerr << "Polyhedron_scan_OFF<HDS>::" << std::endl;
            std::cerr << "operator(): input error: file format is not in "
                         "OFF." << std::endl;
        }
        return;
    }
    m_file_header = scanner;  // Remember file header after return.

    Polyhedron_incremental_builder_3<HDS> B( target, scanner.verbose());
    B.begin_surface( scanner.size_of_vertices(),
                     scanner.size_of_facets(),
                     scanner.size_of_halfedges());

    typedef typename HDS::Traits     Traits;
    typedef typename Traits::Point_3 Point;

    // read in all vertices
    std::size_t  i;
    for ( i = 0; i < scanner.size_of_vertices(); i++) {
        Point p;
        file_scan_vertex( scanner, p);
        B.add_vertex( p);
        scanner.skip_to_next_vertex( i);
    }
    if ( ! m_in  || B.error()) {
        B.rollback();
        m_in.clear( std::ios::badbit);
        return;
    }

    long skipped_facets = 0;
    // read in all facets
    for ( i = 0; i < scanner.size_of_facets(); i++) {
        std::size_t no;
        scanner.scan_facet( no, i); // get number of vertices
        if( ! m_in || B.error()) {
        	if (scanner.verbose()) {
        		std::cerr << " " << std::endl;
        		std::cerr << "Polyhedron_scan_OFF<Traits>::" << std::endl;
        		std::cerr << "Some error occurred foo1" << std::endl;
        		std::cerr << "Skipped " << skipped_facets << " facets" << std::endl;
        	}
            m_in.clear( std::ios::badbit);
            return;
        }
        if (no < 3) {
            if ( scanner.verbose()) {
                std::cerr << " " << std::endl;
                std::cerr << "Polyhedron_scan_OFF<Traits>::" << std::endl;
                std::cerr << "operator()(): skipping: facet " << i
                     << " has less than 3 vertices." << std::endl;
            }
            continue;
        }

        // Record facet indices
        std::vector<std::size_t> v;
        for ( std::size_t j = 0; j < no; j++) {
            std::size_t index;
            scanner.scan_facet_vertex_index( index, i);
            v.push_back(index);
        }

        // Test new facet before adding
        if (B.test_facet(v.cbegin(), v.cend())) {
        	B.begin_facet();
        	for (auto it = v.begin(); it != v.end(); it++) {
        		B.add_vertex_to_facet(*it);
        	}
        	B.end_facet();
        } else {
        	if (scanner.verbose()) {
                std::cerr << " " << std::endl;
                std::cerr << "Polyhedron_scan_OFF<Traits>::" << std::endl;
                std::cerr << "operator()(): facet test failed: facet " << i
                     << "... trying reverse" << std::endl;
        	}

        	// Try reversing if not successful
        	std::reverse(v.begin(), v.end());
        	if (B.test_facet(v.cbegin(), v.cend())) {
        		B.begin_facet();
        		for (auto it = v.begin(); it != v.end(); it++) {
        			B.add_vertex_to_facet(*it);
        		}
        		B.end_facet();
        	} else {
        		// If reverse fails too, skip the facet
        		if (scanner.verbose()) {
        			std::cerr << " " << std::endl;
        			std::cerr << "Polyhedron_scan_OFF<Traits>::" << std::endl;
        			std::cerr << "operator()(): facet reverse test failed: facet " << i
        					<< "... SKIPPING" << std::endl;
        		}
        		++skipped_facets;
        	}
        }
        scanner.skip_to_next_facet( i);
    }
	std::cerr << " " << std::endl;
	std::cerr << "Polyhedron_scan_OFF<Traits>::" << std::endl;
	std::cerr << "operator()(): skipped" << skipped_facets
			<< " facets" << std::endl;
    if ( ! m_in  || B.error()) {
        B.rollback();
        m_in.clear( std::ios::badbit);
        return;
    }
/*    if ( B.check_unconnected_vertices()) {
        if ( ! B.remove_unconnected_vertices()) {
            if ( scanner.verbose()) {
                std::cerr << " " << std::endl;
                std::cerr << "Polyhedron_scan_OFF<Traits>::" << std::endl;
                std::cerr << "operator()(): input error: cannot "
                             "successfully remove isolated vertices."
                          << std::endl;
            }
            B.rollback();
            m_in.clear( std::ios::badbit);
            return;
        }
    }*/
    B.end_surface();
}

template < class Traits,
           class Items,
           template < class T, class I, class A>
           class HDS, class Alloc>
void scan_OFF( std::istream& in, Polyhedron_3<Traits,Items,HDS,Alloc>& P,
               bool verbose = false) {
    // reads a polyhedron from `in' and appends it to P.
    typedef Polyhedron_3<Traits,Items,HDS,Alloc> Polyhedron;
    typedef typename Polyhedron::HalfedgeDS HalfedgeDS;
    typedef Polyhedron_scan_OFF<HalfedgeDS> Scanner;
    Scanner scanner( in, verbose);
    P.delegate(scanner);
}

template < class Traits,
           class Items,
           template < class T, class I, class A>
           class HDS, class Alloc>
std::ostream&
operator<<( std::ostream& out, const Polyhedron_3<Traits,Items,HDS,Alloc>& P) {
    // writes P to `out' in PRETTY, ASCII or BINARY format
    // as the stream indicates.
    File_header_OFF header( is_binary( out), ! is_pretty( out), false);
    CGAL::print_polyhedron_with_header_OFF( out, P, header);
    return out;
}

template < class Traits,
           class Items,
           template < class T, class I, class A>
           class HDS, class Alloc>
std::istream& operator>>(std::istream& in,
                         Polyhedron_3<Traits,Items,HDS,Alloc>& P) {
    // reads a polyhedron from `in' and appends it to P.
    CGAL::scan_OFF( in, P);
    return in;
}

} //namespace CGAL

#endif /* MODIFIED_POLYHEDRON_SCAN_OFF_H_ */

// EOF //
