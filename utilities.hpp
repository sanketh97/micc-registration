#ifndef UTILITIES_HPP_INCLUDED
#define UTILITIES_HPP_INCLUDED

// standard libraries
#include <iterator>
#include <vector>
#include <fstream>
#include <string>

// project
#include "debug.hpp"

template <typename T>
std::vector<T>* read_file_to_vector(std::string filename)
{
    std::ifstream file(filename, std::ifstream::in);
    std::vector<T>* v = new std::vector<T>();

    std::istream_iterator<T> eof;
    std::istream_iterator<T> f(file);

    for (; f != eof; f++)
    {
        v->push_back(*f);
    }

    return v;
}

template <typename T, template <typename, typename...> class V, typename... Args>
void print_container(V<T, Args...> &container, std::ostream& out = std::cout, std::string separator = ", ")
{
    std::copy(container.cbegin(), container.cend(), std::ostream_iterator<T>(out, separator.c_str()));
}

#endif // UTILITIES_HPP_INCLUDED
