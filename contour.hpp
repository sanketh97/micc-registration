#ifndef CONTOUR_HPP_INCLUDED
#define CONTOUR_HPP_INCLUDED

// standard libraries
#include <vector>
#include <boost/property_map/property_map.hpp>

#include <CGAL/Plane_3.h>

#include "preprocess.hpp"

template <class Point_Map_In, typename InputIterator, class Point_Map_Out, typename OutputIterator, class Transformation>
void rotate(Point_Map_In in_map, InputIterator p_begin, InputIterator p_beyond,
		Point_Map_Out out_map, OutputIterator out, const Transformation& t)
{
	typedef typename CGAL::value_type_traits<OutputIterator>::type EnrichedPoint;
	std::for_each(p_begin, p_beyond,
			[t, in_map, out, out_map](typename CGAL::value_type_traits<InputIterator>::type p)
			mutable {EnrichedPoint pt; put(out_map, pt, get(in_map, p).transform(t)); *out++ = pt;});
}

#endif // CONTOUR_HPP_INCLUDED
